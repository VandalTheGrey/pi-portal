#!/bin/bash

# ------------- Initial Variables ----------------

portal_id=0
read -rp "Enter Portal ID: " portal_id
while [[ $portal_id != +([0-9]) ]] || (( portal_id < 1 || portal_id > 254 )); do
  echo "ID must be a number between 1 and 254"
  read -rp "Enter Portal ID: " portal_id
done
edge_address="172.16.0.$portal_id"

read -p "Enter Portal password: " edge_pass

local_updates=false
while true; do
  read -p "Send local routes[y/n]? " yn
  case $yn in 
    [Yy]* ) local_updates=true; break;;
    [Nn]* ) local_updates=false; break;;
  esac
done

echo 

# ------------------------------------------------



# -------------- RANDO BULLSHIT ------------------

install-n2n-deps-centos() {
  yum install -y dh-autoreconf
}

install-n2n-deps-debian() {
  apt-get install -y dh-autoreconf
}

#-------------------------------------------------


compile-n2n() {
  cd /opt/
  git clone https://github.com/ntop/n2n
  cd n2n
  ./autogen.sh
  ./configure
  make
  make install
}


# -------------- Helper Functions ----------------

setup-n2n-debian() {
  os_codename=$(grep '^VERSION_CODENAME=' /etc/os-release | cut -d '=' -f 2 | sed 's/"//g' )
  sed -i '/^deb/ s/$/ contrib/' /etc/apt/sources.list

  if [[ "$os_codename" == "bullseye" ]]; then
    apt-get update; 
    apt-get install -y gnupg;
  fi

  rm "./apt-ntop-stable.$os_codename.deb"
  wget "https://packages.ntop.org/apt-stable/bullseye/all/apt-ntop-stable.deb" -O "/tmp/apt-ntop-stable.deb"
  dpkg -i "/tmp/apt-ntop-stable.deb"

  apt-get clean all
  apt-get update
  apt-get install -y n2n
}

setup-n2n-centos() {
  curl https://packages.ntop.org/centos-stable/ntop.repo > /etc/yum.repos.d/ntop.repo
  version_id=$(grep '^VERSION_ID=' /etc/os-release | cut -d '=' -f 2 | sed 's/"//g' )
  yum install -y epel-release
  if [[ "$version_id" == "8" ]]; then
    # https://www.cyberithub.com/solved-failed-to-download-metadata-for-repo-appstream/
    sed -i 's/mirrorlist/#mirrorlist/g' /etc/yum.repos.d/CentOS-*
    sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' /etc/yum.repos.d/CentOS-*

    rpm -ivh http://rpms.remirepo.net/enterprise/remi-release-8.rpm
    yum install -y dnf-plugins-core
    dnf config-manager --set-enabled PowerTools
    dnf config-manager --set-enabled remi
  fi

  yum clean all
  yum update -y 
  yum install -y n2n
}

setup-n2n-ubuntu() {
  version_id=$(grep '^VERSION_ID=' /etc/os-release | cut -d '=' -f 2 | sed 's/"//g' )
  apt-get install -y software-properties-common wget
  add-apt-repository -y universe
  rm "./apt-ntop-stable.$version_id.deb"
  wget "https://packages.ntop.org/apt-stable/$version_id/all/apt-ntop-stable.deb" -O "/tmp/apt-ntop-stable.deb"
  #apt install -y "./apt-ntop-stable.$version_id.deb"
  dpkg -i "/tmp/apt-ntop-stable.deb"

  apt-get clean all
  apt-get update
  apt-get install -y n2n
}

setup-n2n-raspbian() {
  wget https://packages.ntop.org/RaspberryPi/apt-ntop.deb
  dpkg -i apt-ntop.deb
  apt-get update
  apt-get install -y n2n
}

install-gvm-deps-debian() {
  apt-get install -y curl \
	  git \
	  mercurial \
	  make \
	  binutils \
	  bison \
	  gcc \
	  build-essential \
	  protobuf-compiler
}

install-gvm-deps-centos() {
  yum install -y curl
  yum install -y git
  yum install -y make
  yum install -y bison
  yum install -y gcc
  yum install -y glibc-devel
  yum install -y protobuf-compiler
}

# ----------------------------------------------

os_id=$(grep '^ID=' /etc/os-release | cut -d '=' -f 2 | sed 's/"//g' )
case $os_id in 
  centos) setup-n2n-centos ;;
  debian) setup-n2n-debian ;;
  ubuntu) setup-n2n-ubuntu ;;
  raspbian) setup-n2n-raspbian ;;
  *) echo "Unkown OSID: $os_id"; exit -1;
esac

version=$(grep '^VERSION_ID=' /etc/os-release | cut -d '=' -f 2)
echo "Read Version $version";

case $os_id in 
  centos) install-gvm-deps-centos ;;
  debian) install-gvm-deps-debian ;;
  ubuntu) install-gvm-deps-debian ;;
  raspbian) install-gvm-deps-debian ;;
  *) echo "Unkown OSID: $os_id"; exit -1;
esac


bash < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)

source ~/.gvm/scripts/gvm
gvm install go1.17 -B
gvm use go1.17 --default

if [ ! -f /usr/local/bin/edge_router ]; then
(
  cd edge_router
  ./build.sh
  cp edge_router /usr/local/bin/
  mkdir /etc/edge-router/
)
else
  echo "Edge Router already installed, skipping";
fi

#if [ ! -d "/opt/n2n" ]; then
#(
#  cd /opt/
#  git clone https://github.com/ntop/n2n
#  cd n2n
#  git reset --hard 3.0
#
#  ./autogen.sh
#  ./configure
#  make
#  make install
#
#  mkdir -p /etc/n2n
#)
#else
#  echo "n2n Edge already installed, skipping";
#fi

mkdir /etc/n2n
cp files/edge.service /etc/systemd/system/edge.service
cp files/edge.conf /etc/n2n/edge.conf
sed -i "s/EDGE_ADDRESS/$edge_address/" /etc/n2n/edge.conf
sed -i "s/EDGE_PASS/$edge_pass/" /etc/n2n/edge.conf

cp files/edge-router.service /etc/systemd/system/edge-router.service
cp files/edge_router_config.yaml /etc/edge-router/config.yaml
if [ $local_updates ]; then
  sed -i "s/false/true/" /etc/edge-router/config.yaml
fi;

systemctl daemon-reload

systemctl enable edge.service
service edge start



